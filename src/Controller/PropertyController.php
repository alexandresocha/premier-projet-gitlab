<?php
namespace App\Controller;

use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Property;
use Symfony\Component\Routing\Annotation\Route;

class PropertyController extends AbstractController
{
    /**
     * @var PropertyRepository
     */
    private $repository;


    /**
     * PropertyController constructor.
     * @param PropertyRepository $repository
     * @param EntityManagerInterface $em
     */
    public function __construct(PropertyRepository $repository, EntityManagerInterface $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    public function index(): Response
    {
        $properties = $this->repository->findAllVisible();

        return $this->render('property/index.html.twig', [
            'les_proprietes' => $properties]
        );
    }

    public function show(Property $property, $slug): Response
    {
        if($property->getSlug() !== $slug ){
            return $this->redirectToRoute('property.show', [
                'id' => $property->getId(),
                'slug' => $property->getSlug()
            ], 301);
        }
        return $this->render('property/show.html.twig', [
            'property' => $property,
            'current_menu'=> 'properties'
        ]);

        /*41:34*/
    }

    public function singe(): Response
    {
        $singe = "Singe";
        return $this->render('property/singe.html.twig',
            ['current_menu' => $singe]
        );
    }

}